import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from 'src/app/interfaces/user';
import { AuthService } from '../auth/auth.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) {}


  // GET by id
  async getUser(userId: string) {
    return this.http.get<any>(`${environment.API_URL}/users/${userId}`).toPromise();
  }


  // GET comments by id
  async getComments(userId: string) {
    const headerOptions = this.httpOptions.headers.append('Authorization', `Bearer ${this.auth.userToken}`);
    return this.http.get<any>(`${environment.API_URL}/users/${userId}/comments`, {headers: headerOptions}).toPromise();
  }

  // UPDATE
  async editUser(user: User) {
    const headerOptions = this.httpOptions.headers.append('Authorization', `Bearer ${this.auth.userToken}`);
    return this.http.put<any>(`${environment.API_URL}/users/me`, user, {
      headers: headerOptions
    }).toPromise();
  }

  // 
  async changePassword(password: string) {
    const headerOptions = this.httpOptions.headers.append('Authorization', `Bearer ${this.auth.userToken}`);
    return this.http.put<any>(`${environment.API_URL}/users/me/password`, {"password": password}, {
      headers: headerOptions
    }).toPromise();
  }

  // DELETE
  async deleteUser() {
    const headerOptions = this.httpOptions.headers.append('Authorization', `Bearer ${this.auth.userToken}`);
    return this.http.delete<any>(`${environment.API_URL}/users/me`, {
      headers: headerOptions
    }).toPromise();
  }
}
