import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { AuthService } from '../auth/auth.service';
import { Article } from 'src/app/interfaces/article';


@Injectable({
  providedIn: 'root'
})
export class ArticleService {
  
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(
    private http: HttpClient,
    private auth: AuthService
  ) {}

  // GET by id
  async getArticle(articleId: string) {
    return this.http.get<Article>(`${environment.API_URL}/articles/${articleId}`).toPromise();
  }

  // GET all articles
  async getArticles() {
    return this.http.get<Article[]>(`${environment.API_URL}/articles/`).toPromise();
  }

  async writeComment(articleId: string, text: string) {
    const headerOptions = this.httpOptions.headers.append('Authorization', `Bearer ${this.auth.userToken}`);
    return this.http.post<Article>(`${environment.API_URL}/articles/${articleId}/newcomment`,{'text':text}, {
      headers: headerOptions
    }).toPromise();
  }

  async search(query: string){
    return this.http.get<Article[]>(`${environment.API_URL}/articles/search?query=${query}`).toPromise();
  }

  async saveArticle(article : Article) {
    const headerOptions = this.httpOptions.headers.append('Authorization', `Bearer ${this.auth.userToken}`);
    return this.http.post<Article>(`${environment.API_URL}/articles/new`,{
      'content':article.content,
      'title':article.title,
      'hashtags':article.hashtags
    }, {
      headers: headerOptions
    }).toPromise();
  }


    // UPDATE
    async editArticle(article: Article, id: string) {
      const headerOptions = this.httpOptions.headers.append('Authorization', `Bearer ${this.auth.userToken}`);
      return this.http.put<any>(`${environment.API_URL}/articles/${id}`, article, {
        headers: headerOptions
      }).toPromise();
    }
}
