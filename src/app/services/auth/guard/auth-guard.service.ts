import { Injectable } from '@angular/core';
import { CanActivate, Router, UrlTree } from '@angular/router';
import { AuthService } from '../auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private auth: AuthService, private router: Router) { }
  canActivate() {
    return this.auth.isLoggedIn().then((isLoggedIn) => {
      if (isLoggedIn)
        return true;
      else
        return this.router.parseUrl('/login');
    })



  }

}
