import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Login, LoginResponse } from 'src/app/interfaces/login';
import { User } from 'src/app/interfaces/user';
import { BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  public me = {} as User;
  public userToken: string;

  constructor(
    private platform: Platform,
    private http: HttpClient,
    private storage: Storage
  ) {

    this.saveToken().then(() => {
      if (this.userToken)
        this.updateUserInformations();
    });

  }

  async login(body: Login): Promise<void> {

    // Wrappo l'observable di una chiamata login dentro una promise.
    return new Promise<void>((resolve, reject) => {
      const sub = this.http.post<LoginResponse>(`${environment.API_URL}/auth/login`, body, this.httpOptions)
        .subscribe(async res => {
          await this.saveTokenToLocalStorage(res);
          await this.saveToken();
          sub.unsubscribe();
          await this.updateUserInformations();
          console.log(this.me);
          resolve();
        }, reject);

    });

  }

  // ME
  async getMe() {
    const headerOptions = this.httpOptions.headers.append('Authorization', `Bearer ${this.userToken}`);
    return this.http.get<User>(`${environment.API_URL}/users/me`, {
      headers: headerOptions
    }).toPromise();
  }


  async register(user: User) {
    return this.http.post<User>(`${environment.API_URL}/auth/register`, user, this.httpOptions).toPromise();
  }

  isLoggedIn() {
    return this.saveToken()
            .then(() => {this.updateUserInformations();})
            .then(() => {return this.userToken;})
  }

  async saveToken() {
    await this.storage.get("ACCESS_TOKEN").then((response) => {
      this.userToken = response;
    });
  }

  async updateUserInformations() {
    try{
      this.me = await this.getMe();
    }
    catch(error){
      this.userToken = null;
      this.logout();
    }
      
  }

  async logout() {
    await this.storage.remove("ACCESS_TOKEN");
    await this.storage.remove("EXPIRES_IN");
  }


  async saveTokenToLocalStorage(res: LoginResponse) {
    await this.storage.set("ACCESS_TOKEN", res.accessToken);
    await this.storage.set("EXPIRES_IN", res.expiresIn);
  }



}
