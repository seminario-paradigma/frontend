import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
})

export class MenuComponent implements OnInit {
  private isLoggedIn;
  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Posts',
      url: '/posts',
      icon: 'newspaper'
    },
    {
      title: 'Profile',
      url: '/profile',
      icon: 'person'
    }
  ];

  constructor(private auth: AuthService) {}


  async ngOnInit() {
    this.isLoggedIn = await this.auth.isLoggedIn();
    const path = window.location.pathname.split('/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
}

