import { NgModule } from '@angular/core';
import { IonicModule} from '@ionic/angular';


import { HttpClientModule } from '@angular/common/http';
import { MenuComponent } from './menu.component';
import { MenuRoutingModule } from './menu-routing.module';
import { CommonModule } from "@angular/common";

@NgModule({
  imports: [
    IonicModule,
    MenuRoutingModule,
    HttpClientModule,
    CommonModule
  ],
  declarations: [MenuComponent]
})
export class MenuModule {}



