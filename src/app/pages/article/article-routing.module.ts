import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArticlePage } from './article.page';

const routes: Routes = [
  {
    path: '',
    component: ArticlePage
  },  {
    path: 'edit-article-modal',
    loadChildren: () => import('./edit-article-modal/edit-article-modal.module').then( m => m.EditArticleModalPageModule)
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ArticlePageRoutingModule {}
