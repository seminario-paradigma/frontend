import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditArticleModalPage } from './edit-article-modal.page';

const routes: Routes = [
  {
    path: '',
    component: EditArticleModalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditArticleModalPageRoutingModule {}
