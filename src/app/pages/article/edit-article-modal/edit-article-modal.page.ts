import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { ArticleService } from 'src/app/services/article/article.service';
import { Article } from 'src/app/interfaces/article';

@Component({
  selector: 'app-edit-article-modal',
  templateUrl: './edit-article-modal.page.html',
  styleUrls: ['./edit-article-modal.page.scss'],
})
export class EditArticleModalPage implements OnInit {

  article:Article = {};

  constructor(
    private modalController: ModalController,
    private articleService: ArticleService
  ) { }

  dismiss() {

    this.modalController.dismiss({
      'dismissed': true
    });
  }
  
  ngOnInit() {
  }

  async save(id) {
    try {
      console.log("art" + this.article);
      console.log("id:" + id);
      await this.articleService.editArticle(this.article, id);
    } catch (err) {}
    this.dismiss();
  }

}
