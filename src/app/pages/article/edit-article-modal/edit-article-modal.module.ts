import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditArticleModalPageRoutingModule } from './edit-article-modal-routing.module';

import { EditArticleModalPage } from './edit-article-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditArticleModalPageRoutingModule
  ],
  declarations: [EditArticleModalPage]
})
export class EditArticleModalPageModule {}
