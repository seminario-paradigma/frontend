import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/article';
import {User} from 'src/app/interfaces/user';
import { ArticleService } from 'src/app/services/article/article.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router,ActivatedRoute } from '@angular/router';
import { IonicToastService } from '../services/ionic-toast.service';
import { NavController, ModalController } from '@ionic/angular';
import { EditArticleModalPage } from './edit-article-modal/edit-article-modal.page';
import { UserService } from 'src/app/services/user/user.service';




@Component({
  selector: 'app-article',
  templateUrl: './article.page.html',
  styleUrls: ['./article.page.scss'],
})
export class ArticlePage implements OnInit {

  article: Article ={};
  data: any;
  newComment: string;
  commentsName: string[] = [];
  logged:string ="none";
  constructor(
    private articlesService: ArticleService,
    private auth: AuthService,
    private userService : UserService,
    private route: ActivatedRoute, 
    private router: Router,
    private ionicToastService: IonicToastService,
    public modalController: ModalController
  ) {
    
  }

   async ngOnInit() {
    this.logged = await this.auth.isLoggedIn();
    this.route.queryParams.subscribe(params => {
      if (params && params.id) {
        this.data = JSON.parse(params.id);
      }
    });
    await this.getArticle(this.data);
  }

   async getArticle(data) {
    try {
      this.article=await this.articlesService.getArticle(data);
      this.article.hashtagsArray = this.article.hashtags.split(" ");
      await this.article.comments.forEach(element => {
        this.userService.getUser(element.from).then(res=> {
          this.commentsName.push(res.username);
        });
      });
    } catch (err) {
    }
  }

  async writeComment(id) {
    this.article = await this.articlesService.writeComment(id,this.newComment);
    this.newComment="";
    this.showMyToast("Commento creato!")
  }

  showMyToast(msg : string) {
    this.ionicToastService.showToast(msg);
  }
  hideMyToast() {
    this.ionicToastService.HideToast();
  }

  async showEditArticle(article) {
    console.log(article);
    const modal = await this.modalController.create({
      component: EditArticleModalPage,
      componentProps: {
        article: article
     }
    });
    
    // modal.onDidDismiss().then( async () => {
    //   //this.articles = await this.articlesService.getArticles();
    // });
    await modal.present();

  }

}
