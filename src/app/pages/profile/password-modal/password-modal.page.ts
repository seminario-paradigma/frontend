import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UserService } from 'src/app/services/user/user.service';
import { IonicToastService } from '../../services/ionic-toast.service';

@Component({
  selector: 'app-password-modal',
  templateUrl: './password-modal.page.html',
  styleUrls: ['./password-modal.page.scss'],
})
export class PasswordModalPage implements OnInit {

  private newPassword: string;
  private confirmedPassword: string;
  constructor(
    private modalController: ModalController,
    private auth: AuthService,
    private users: UserService,
    private toastService: IonicToastService
  ) {
    this.auth.updateUserInformations();
    
  }
  dismiss() {

    this.modalController.dismiss({
      'dismissed': true
    });
  }
  ngOnInit() {
  }


  async save() {

    try {
      if(this.newPassword === this.confirmedPassword){
        await this.users.changePassword(this.newPassword);
        this.dismiss();
      }
      else
      await this.toastService.showToast("Your passwords don't match. Try again");
      
      

    } catch (err) {
      await this.toastService.showToast(err.message);

    }
    
  }



}
