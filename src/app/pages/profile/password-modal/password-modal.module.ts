import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PasswordModalPageRoutingModule } from './password-modal-routing.module';

import { PasswordModalPage } from './password-modal.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasswordModalPageRoutingModule
  ],
  declarations: [PasswordModalPage]
})
export class PasswordModalPageModule {}
