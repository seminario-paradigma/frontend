import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UserService } from 'src/app/services/user/user.service';
import { ModalController } from '@ionic/angular';
import { User } from 'src/app/interfaces/user';
import { LoginResponse } from 'src/app/interfaces/login';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.page.html',
  styleUrls: ['./edit-modal.page.scss'],
})
export class EditModalPage implements OnInit {

  private me: User;
  constructor(
    private modalController: ModalController,
    private auth: AuthService,
    private users: UserService
  ) {
    this.auth.updateUserInformations();
    this.me = this.auth.me;
    
  }
  dismiss() {

    this.modalController.dismiss({
      'dismissed': true
    });
  }
  ngOnInit() {
  }


  async save() {

    try {
      let userToken: LoginResponse = await this.users.editUser(this.me);
      await this.auth.saveTokenToLocalStorage(userToken);
      await this.auth.saveToken();
      await this.auth.updateUserInformations();
      

    } catch (err) {

    }
    this.dismiss();
  }



}
