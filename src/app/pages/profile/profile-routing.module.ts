import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilePage } from './profile.page';
import { AuthGuardService } from 'src/app/services/auth/guard/auth-guard.service';

const routes: Routes = [
  {
    path: '',
    component: ProfilePage,
    canActivate: [AuthGuardService]
  },
  {
    path: ':id',
    component: ProfilePage
  },
  {
    path: 'comments-modal',
    loadChildren: () => import('./comments-modal/comments-modal.module').then( m => m.CommentsModalPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'edit-modal',
    loadChildren: () => import('./edit-modal/edit-modal.module').then( m => m.EditModalPageModule),
    canActivate: [AuthGuardService]
  },
  {
    path: 'password-modal',
    loadChildren: () => import('./password-modal/password-modal.module').then( m => m.PasswordModalPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilePageRoutingModule {}
