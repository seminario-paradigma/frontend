import { Component, OnInit, Input } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { UserService } from 'src/app/services/user/user.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UserComment } from 'src/app/interfaces/comment';
import { IonicToastService } from '../../services/ionic-toast.service';

@Component({
  selector: 'app-comments-modal',
  templateUrl: './comments-modal.page.html',
  styleUrls: ['./comments-modal.page.scss'],
})
export class CommentsModalPage implements OnInit {

  private comments: Array<UserComment>;
  userId: string;
  constructor(
    private modalController: ModalController,
    private navParams: NavParams,
    private user: UserService,
    private toastService: IonicToastService
  ) {
    this.userId = this.navParams.get('userId');
  
      this.user.getComments(this.userId)
      .then((response) => {
          this.comments = response;
      }).catch(err => this.toastService.showToast(err.error));
  
    
    
  }
  dismiss() {

    this.modalController.dismiss({
      'dismissed': true
    });
  }
  ngOnInit() {
  }

}
