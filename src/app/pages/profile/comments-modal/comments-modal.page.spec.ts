import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CommentsModalPage } from './comments-modal.page';

describe('CommentsModalPage', () => {
  let component: CommentsModalPage;
  let fixture: ComponentFixture<CommentsModalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommentsModalPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CommentsModalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
