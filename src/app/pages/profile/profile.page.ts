import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { AuthService } from 'src/app/services/auth/auth.service';
import { UserService } from 'src/app/services/user/user.service';
import { LoginResponse } from 'src/app/interfaces/login';
import { NavController, ModalController } from '@ionic/angular';
import { CommentsModalPage } from './comments-modal/comments-modal.page';
import { EditModalPage } from './edit-modal/edit-modal.page';
import { ActivatedRoute } from '@angular/router';
import { AlertService } from 'src/app/services/alert/alert.service';
import { PasswordModalPage } from './password-modal/password-modal.page';
import { IonicToastService } from '../services/ionic-toast.service';


@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  private user = {} as User;
  private isMyProfile: boolean;
  private isLoggedIn;
  constructor(
    private auth: AuthService,
    private users: UserService,
    private navCtrl: NavController,
    public modalController: ModalController,
    private route: ActivatedRoute,
    private alert: AlertService,
    private toastService: IonicToastService
  ) {    
  }

  async ngOnInit() {
    await this.auth.updateUserInformations();
    const userId = this.route.snapshot.paramMap.get('id');
    if(userId) this.user = await this.users.getUser(userId);
    else this.user = this.auth.me;
    if(this.user._id == this.auth.me._id){
      this.isMyProfile = true;
    }

    this.isLoggedIn = await this.auth.isLoggedIn();
      
  }

  
  async confirmDelete() {
    try {
      await this.users.deleteUser();
      await this.toastService.showToast("Your account has been succesfully deleted");
      await this.logout();
    } catch (err) {
      await this.toastService.showToast(err.message);
    }

  }

  async logout() {
    await this.auth.logout();
    this.navCtrl.navigateRoot("/home");
  }


  async showCommentsModal() {
    if(this.isLoggedIn){
      const modal = await this.modalController.create({
        component: CommentsModalPage,
        componentProps: {
          "userId": this.user._id
        }
      });
      return await modal.present();

    }
    else this.navCtrl.navigateForward("/login");
    
  }

  async showEditModal() {
    const modal = await this.modalController.create({
      component: EditModalPage
    });

    modal.onDidDismiss().then( async () => {
      this.auth.updateUserInformations();
      this.user = this.auth.me;
    });
    return await modal.present();
  }

  async showPasswordModal() {
    const modal = await this.modalController.create({
      component: PasswordModalPage
    });

    modal.onDidDismiss().then( async () => {
      this.auth.updateUserInformations();
      this.user = this.auth.me;
    });
    return await modal.present();
  }

async delete() {
    const alertButtons = [{
      text: 'Back',
      role: 'cancel',
      handler: () => {}
    }, {
      text: 'Delete',
      handler: async () => {
        this.confirmDelete();
      }
    }];

    await this.alert.show({
      header: 'Delete',
      message: 'This action can not be undone. Are you sure you want to delete your account?',
      buttons: alertButtons
    });

  }



}
