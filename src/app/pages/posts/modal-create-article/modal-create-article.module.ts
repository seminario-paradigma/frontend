import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalCreateArticlePageRoutingModule } from './modal-create-article-routing.module';

import { ModalCreateArticlePage } from './modal-create-article.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalCreateArticlePageRoutingModule
  ],
  declarations: [ModalCreateArticlePage]
})
export class ModalCreateArticlePageModule {}
