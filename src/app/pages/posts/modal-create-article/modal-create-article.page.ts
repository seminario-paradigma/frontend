import { Component, OnInit } from '@angular/core';
import { ModalController, NavParams } from '@ionic/angular';
import { Article } from 'src/app/interfaces/article';
import { ArticleService } from 'src/app/services/article/article.service';


@Component({
  selector: 'app-modal-create-article',
  templateUrl: './modal-create-article.page.html',
  styleUrls: ['./modal-create-article.page.scss'],
})
export class ModalCreateArticlePage implements OnInit {

  private article : Article={};
  constructor(
    private modalController: ModalController,
    private articleService: ArticleService,
    private navParams: NavParams
  ) { }

  dismiss() {

    this.modalController.dismiss({
      'dismissed': true
    });
  }

  ngOnInit() {
    const article: string = this.navParams.get('article');
  }

  async save() {
    try {
      await this.articleService.saveArticle(this.article);
    } catch (err) {}
    this.dismiss();
  }
}
