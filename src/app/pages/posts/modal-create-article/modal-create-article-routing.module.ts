import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalCreateArticlePage } from './modal-create-article.page';

const routes: Routes = [
  {
    path: '',
    component: ModalCreateArticlePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalCreateArticlePageRoutingModule {}
