import { Component, OnInit } from '@angular/core';
import { Article } from 'src/app/interfaces/article';
import { ArticleService } from 'src/app/services/article/article.service';
import { AuthService } from 'src/app/services/auth/auth.service';
import { Router, NavigationExtras } from '@angular/router';
import { CompileTemplateMetadata } from '@angular/compiler';
import { NavController, ModalController } from '@ionic/angular';
import { ModalCreateArticlePage } from './modal-create-article/modal-create-article.page';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.page.html',
  styleUrls: ['./posts.page.scss'],
})
export class PostsPage implements OnInit {

  articles: Article[] = [];
  hideme = [];
  logged:string ="none";
  newest:boolean=true;
  searchStr:string;

  constructor(
    private articlesService: ArticleService,
    private auth: AuthService,
    private router: Router,
    public modalController: ModalController
  ) {}

  async ngOnInit() {
    this.logged = await this.auth.isLoggedIn();
    await this.doRefresh(null);
    await this.resetState();
  }

  async ionViewWillEnter(){
    console.log("ionViewWillEnter")
    await this.resetState();
}

  async doRefresh(event) {
    await this.getArticles();
    if(event!=null)
      event.target.complete();
  }

  order() {
    if(this.newest)
    this.articles=this.orderNewest(this.articles);
  else
    this.articles=this.orderOldest(this.articles);
  }

  changeOrder() {
    this.newest=!this.newest;
    this.resetState();
    this.order();
    }

    resetState() {
      for (let i=0;i<this.articles.length;i++) {
        this.hideme[i]='none';
      }
    }

  async getArticles() {
    try {
      this.articles = await this.articlesService.getArticles();
      await this.resetState();
      await this.order();
    } catch (err) {}
  }

  orderNewest(arr) {
    arr.sort((a,b)=>(a.created_at<b.created_at)?1:-1);
    return arr;
  }

  async filterList(evt) {
    var query=evt.srcElement.value;
    console.log(query);
    if(query=="")
      this.doRefresh(null);
    else
      this.articles = await this.articlesService.search(evt.srcElement.value);
    console.log(this.searchStr);
  }

  resetSearch(evt) {
    this.searchStr="";
    this.doRefresh(null);
  }

  orderOldest(arr) {
    arr.sort((a,b)=>(a.created_at>b.created_at)?1:-1);
    return arr;
  }

  clicked(i,id) {
    console.log(id);
    if(this.hideme[i]==false)
    {
      let navigationExtras: NavigationExtras = {
        queryParams: {
          id: JSON.stringify(id)
        }
      };
      this.router.navigate(['article'],navigationExtras);
    } 
    else
      this.hideme[i] = !this.hideme[i];
  }

  async showArticleModal() {
    const modal = await this.modalController.create({
      component: ModalCreateArticlePage,
    });
    
    modal.onDidDismiss().then( async () => {
      this.articles = await this.articlesService.getArticles();
    });
    await modal.present();

  }

  async searchHash(evt) {
    var query=evt.srcElement.textContent;
    query = query.substr(1);
    console.log("i" + query);
    this.articles = await this.articlesService.search(query);
    console.log(this.searchStr);
    this.searchStr = await query;
    console.log(this.searchStr);
  }
}
