import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { User } from 'src/app/interfaces/user';
import { Login } from 'src/app/interfaces/login';
import { IonicToastService } from '../services/ionic-toast.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss', '../login/login.page.scss'],
})
export class SignupPage {
  private user = {} as User;

  constructor(
    private navCtrl: NavController,
    private auth: AuthService,
    private toastService: IonicToastService
  ) { }

  ngOnInit() {
  }

  async signup() {

    try {

      // Creo utente
      await this.auth.register(this.user);

      // Creo oggetto login
      const loginUser: Login = {
        email: this.user.email,
        password: this.user.password
      };

      await this.auth.login(loginUser);

      // Se la chiamata è andata buon fine, porto l'utente sulla schermata Tabs
      await this.navCtrl.navigateRoot('/');

      await this.toastService.showToast('Account successfully created!');

    } catch (err) {
      await this.toastService.showToast(err.message);

    }


  }

}
