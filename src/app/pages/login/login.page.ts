import { Component, OnInit } from '@angular/core';
import { Login } from 'src/app/interfaces/login';
import { NavController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth/auth.service';
import { IonicToastService } from '../services/ionic-toast.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage {

  loginUser = {} as Login;
  constructor(
    private navCtrl: NavController,
    private auth: AuthService,
    private toastService: IonicToastService
  ) { }


  async login() {

    try {
      await this.auth.login(this.loginUser);
      await this.navCtrl.navigateRoot('/');

    } catch (err) {
      await this.toastService.showToast(err.message);

    }

  

  }

}
