export interface UserComment {
    _id: string,
    comments: {
        text: string;
        from: string;
        last_modified: Date;
        created_at: Date;
        _id: string;
    }
}