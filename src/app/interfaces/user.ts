export interface User {
    email: string;
    password?: string;
    firstname: string;
    lastname: string;
    gender: string;
    profile_image: string;
    birth_date: string;
    lastseen: Date;
    _id?: string;
}