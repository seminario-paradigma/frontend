export interface Article {
    title?: string,
    description?: string
    comments?: any,
    written_by?:string,
    hashtags?:string,
    hashtagsArray?:string[],
    content?:string,
    created_at?:string,
    id?:any
}